package com.kart.service;

import java.util.List;

import com.kart.entity.Order;


public interface OrderService {
	
	List<Order> getAllOrders();

	List<Order> findAllByIds(Iterable<Long> ids);

	Order saveAndFlush(Order order);
	
	List<Order> saveAll(Iterable<Order> orders);
	
	void flush();
	
	Order getOne(Long id);
	
	List<Order> findByOrderName(String orderName);
	
	void deleteInBatch(Iterable<Order> orders);

	void deleteAllInBatch();
	
}
