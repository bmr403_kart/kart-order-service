/**
 * 
 */
package com.kart.service.Impl;

import java.util.List;
  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kart.dao.OrderRepository;
import com.kart.entity.Order;
import com.kart.service.OrderService;


/**
 * @author LENOVO
 *
 */

@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	private OrderRepository orderRepository;

	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	}

	public List<Order> findAllByIds(Iterable<Long> ids) {
		return orderRepository.findAllById(ids);
	}
	
	public List<Order> findByOrderName(String orderName) {
		return orderRepository.findByOrderName(orderName);
	}
	
	public Order getOne(Long id) {
		return orderRepository.getOne(id);
	}
	
	public Order saveAndFlush(Order order) {
		return orderRepository.saveAndFlush(order);
	}

	public void flush() {
		orderRepository.flush();
	}
	
	public List<Order> saveAll(Iterable<Order> orders) {
		return orderRepository.saveAll(orders);
	}

	public void deleteInBatch(Iterable<Order> orders) {
		orderRepository.deleteInBatch(orders);
	}

	public void deleteAllInBatch() {
		orderRepository.deleteAllInBatch();
	}
	
}
